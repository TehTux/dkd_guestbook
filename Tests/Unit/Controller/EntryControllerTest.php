<?php
namespace Dkd\DkdGuestbook\Tests\Unit\Controller;

/**
 * Test case.
 */
class EntryControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Dkd\DkdGuestbook\Controller\EntryController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Dkd\DkdGuestbook\Controller\EntryController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllEntriesFromRepositoryAndAssignsThemToView()
    {

        $allEntries = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entryRepository = $this->getMockBuilder(\Dkd\DkdGuestbook\Domain\Repository\EntryRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $entryRepository->expects(self::once())->method('findAll')->will(self::returnValue($allEntries));
        $this->inject($this->subject, 'entryRepository', $entryRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('entries', $allEntries);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenEntryToEntryRepository()
    {
        $entry = new \Dkd\DkdGuestbook\Domain\Model\Entry();

        $entryRepository = $this->getMockBuilder(\Dkd\DkdGuestbook\Domain\Repository\EntryRepository::class)
            ->setMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $entryRepository->expects(self::once())->method('add')->with($entry);
        $this->inject($this->subject, 'entryRepository', $entryRepository);

        $this->subject->createAction($entry);
    }
}
