
plugin.tx_dkdguestbook_listentries {
    view {
        # cat=plugin.tx_dkdguestbook_listentries/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:dkd_guestbook/Resources/Private/Templates/
        # cat=plugin.tx_dkdguestbook_listentries/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:dkd_guestbook/Resources/Private/Partials/
        # cat=plugin.tx_dkdguestbook_listentries/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:dkd_guestbook/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_dkdguestbook_listentries//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_dkdguestbook_createentry {
    view {
        # cat=plugin.tx_dkdguestbook_createentry/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:dkd_guestbook/Resources/Private/Templates/
        # cat=plugin.tx_dkdguestbook_createentry/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:dkd_guestbook/Resources/Private/Partials/
        # cat=plugin.tx_dkdguestbook_createentry/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:dkd_guestbook/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_dkdguestbook_createentry//a; type=string; label=Default storage PID
        storagePid =
    }
}
