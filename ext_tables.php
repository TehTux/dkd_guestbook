<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Dkd.DkdGuestbook',
            'Listentries',
            'List entries'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Dkd.DkdGuestbook',
            'Createentry',
            'Create entry'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('dkd_guestbook', 'Configuration/TypoScript', 'dkd Guestbook');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_dkdguestbook_domain_model_entry', 'EXT:dkd_guestbook/Resources/Private/Language/locallang_csh_tx_dkdguestbook_domain_model_entry.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dkdguestbook_domain_model_entry');

    }
);
