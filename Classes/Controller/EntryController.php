<?php
namespace Dkd\DkdGuestbook\Controller;

/***
 *
 * This file is part of the "dkd Guestbook" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * EntryController
 */
class EntryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * entryRepository
     *
     * @var \Dkd\DkdGuestbook\Domain\Repository\EntryRepository
     * @inject
     */
    protected $entryRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $entries = $this->entryRepository->findAll();
        $this->view->assign('entries', $entries);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {

    }

    /**
     * action create
     *
     * @param \Dkd\DkdGuestbook\Domain\Model\Entry $newEntry
     * @return void
     */
    public function createAction(\Dkd\DkdGuestbook\Domain\Model\Entry $newEntry)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->entryRepository->add($newEntry);
        $this->redirect('list');
    }
}
