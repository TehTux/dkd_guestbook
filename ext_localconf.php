<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Dkd.DkdGuestbook',
            'Listentries',
            [
                'Entry' => 'list'
            ],
            // non-cacheable actions
            [
                'Entry' => 'create'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Dkd.DkdGuestbook',
            'Createentry',
            [
                'Entry' => 'new, create'
            ],
            // non-cacheable actions
            [
                'Entry' => 'create'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    listentries {
                        iconIdentifier = dkd_guestbook-plugin-listentries
                        title = LLL:EXT:dkd_guestbook/Resources/Private/Language/locallang_db.xlf:tx_dkd_guestbook_listentries.name
                        description = LLL:EXT:dkd_guestbook/Resources/Private/Language/locallang_db.xlf:tx_dkd_guestbook_listentries.description
                        tt_content_defValues {
                            CType = list
                            list_type = dkdguestbook_listentries
                        }
                    }
                    createentry {
                        iconIdentifier = dkd_guestbook-plugin-createentry
                        title = LLL:EXT:dkd_guestbook/Resources/Private/Language/locallang_db.xlf:tx_dkd_guestbook_createentry.name
                        description = LLL:EXT:dkd_guestbook/Resources/Private/Language/locallang_db.xlf:tx_dkd_guestbook_createentry.description
                        tt_content_defValues {
                            CType = list
                            list_type = dkdguestbook_createentry
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'dkd_guestbook-plugin-listentries',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:dkd_guestbook/Resources/Public/Icons/user_plugin_listentries.svg']
			);
		
			$iconRegistry->registerIcon(
				'dkd_guestbook-plugin-createentry',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:dkd_guestbook/Resources/Public/Icons/user_plugin_createentry.svg']
			);
		
    }
);
